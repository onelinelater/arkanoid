// содежимое index.js
const http = require('http');
const path = require('path');
const fs = require('fs');
const PORT = 3000;

const requestHandler = (req, res) => {
	console.log(req.url);
	if (req.url === '/') {
		res.writeHead(200, {'Content-Type': 'text/html'});
		fs.createReadStream(path.join(__dirname, 'index.html')).pipe(res);
	} else if (req.url.indexOf('css') !== -1) {
		res.setHeader('Content-type', 'text/css');
		fs.createReadStream(path.join(__dirname, req.url)).pipe(res);
	} else if (req.url.indexOf('js') !== -1) {
		res.setHeader('Content-type', 'text/javascript');
		if (path.extname(req.url)) {
			fs.createReadStream(path.join(__dirname, req.url)).pipe(res);
		} else {
			fs.createReadStream(path.join(__dirname, req.url) + '.js').pipe(res);
		}
	} else if (req.url.indexOf('resources') !== -1) {
		fs.createReadStream(path.join(__dirname, req.url)).pipe(res);
	} else if (req.url.indexOf('favicon') !== -1) {
		fs.createReadStream(
			path.join(__dirname, 'resources/images/arcanoid.png'),
		).pipe(res);
	} else {
		console.warn(req.url);
		res.write('invalid request');
	}
};

const server = http.createServer(requestHandler);

server.listen(PORT, (err) => {
	if (err) {
		return console.log('something bad happened', err);
	}
	console.log(`server is listening on ${PORT}`);
});
