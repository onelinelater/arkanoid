import * as existingStages from './stages/index.js';
import {DEFAULT_STAGE} from './defaults.js';

export let changeStage = null;

window.onload = ()=>{
	let currentStage = null;

	changeStage = function(stageName, config) {
		currentStage.stop();
		currentStage = null;
		if (!stages.hasOwnProperty(stageName)) {
			throw new Error(`There is no stage ${stageName}!`);
		}
		currentStage = stages[stageName];
		currentStage.start(config);
	};
	const stages = {};
	// eslint-disable-next-line guard-for-in
	for (const stage in existingStages) {
		stages[existingStages[stage].stage] = new existingStages[stage]();
	}
	currentStage = stages[DEFAULT_STAGE];
	currentStage.start();
};
