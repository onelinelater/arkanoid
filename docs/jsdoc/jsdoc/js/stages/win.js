import {Stage} from './abstract';
import {changeStage} from '../index.js';
import {GAME_STAGE, WIN_STAGE} from '../defaults';


/**
 * Класс "Этап "Уровень пройден"".
 *
 * @alias WinStage
 * @augments Stage
 * @memberof stages
 */
export class WinStage extends Stage {
	static stage = WIN_STAGE;

	static transitions = [
		GAME_STAGE,
	]

	/** @inheritdoc */
	constructor() {
		super();
	}

	/** @inheritdoc */
	start(config) {
		this.level = config.level;
		this.$parent.innerHTML = '<h1 style="padding-top: 200px;">' +
			'LEVEL COMPLETE</h1>' +
			'<button id="start" class="play-button">Next level</button>\n';
		this.initKeyControl();
	}

	/** @inheritdoc */
	stop() {
		this.$parent.innerHTML = '';
	}

	/** @inheritdoc */
	initKeyControl() {
		const btn = document.getElementById('start');
		const self = this;
		btn.onclick = function() {
			changeStage(GAME_STAGE, {level: self.level+1});
		};
	}
}
