import {MAIN_FRAME_ID} from '../defaults';


/**
 * Класс "Абстрактный этап игры".
 * @abstract
 * @memberof stages
 * @alias Stage
 */
export class Stage {
	static stage = ''; // Ключевое слово текущего этапа

	static transitions = [] // Возможные переходы на этапы

	/**
	 * Конструктор. Добавляет ссылку на DOM объект, где встроена игровая логика
	 */
	constructor() {
		this.$parent = document.getElementById(MAIN_FRAME_ID);
	}

	/**
	 * Обязательный метод, который начинает этап игры
	 *
	 * @param {Object} config объект с дополнительными данными для этапа
	 */
	start(config) {
		throw new Error('You must realize this method!');
	}

	/**
	 * Обязательный метод, который заканчивает этап игры.
	 * Должен очистить все созданные данные.
	 */
	stop() {
		throw new Error('You must realize this method!');
	}

	/**
	 * Инициализировать управление в рамках этапа
	 */
	initKeyControl() {
	}
}
