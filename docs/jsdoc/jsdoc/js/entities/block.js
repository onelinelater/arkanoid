import {Object} from './object';

/**
 * Класс "Блок".
 * @alias Block
 * @augments Object
 * @memberof entities
 */
export class Block extends Object {
	static options = {
		1: 'one',
		2: 'two',
		3: 'three',
	}

	/** @inheritdoc */
	constructor(id, x, y, value) {
		super(id, x, y, value);
	}

	/**
	 * Добавляет css класс в зависимости от значения.
	 * Значение маппится в словарь класса, который содержит доступные варианты.
	 *
	 * @inheritdoc
	 */
	create(id) {
		const el = super.create(id);
		el.classList.add(Block.options[this.value]);
		return el;
	}

	/**
	 * Добавляет анимамацию "переворота"
	 *
	 * @inheritdoc
	 */
	hit(villain) {
		this.$el.classList.add('hit');
		const self = this;
		setTimeout(() => {
			self.$el.classList.remove(Block.options[this.value]);
			super.hit(villain);
			self.$el.classList.remove('hit');
			self.$el.classList.add(Block.options[this.value]);
		}, 500);
	}
}
