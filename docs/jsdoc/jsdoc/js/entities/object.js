import {Vector} from './vector';
import {
	MAIN_FRAME_ID,
	BOX_SIZE,
	DEFAULT_BORDER_WIDTH_X,
	DEFAULT_BORDER_WIDTH_Y,
	hitPlatformSound,
	hitBlockSound,
} from '../defaults';


/**
 * Класс "Абстрактный объекта мира".
 * @alias Object
 * @memberof entities
 * @abstract
 */
export class Object {
	/**
	 * Конструктор. Размер объекта жёстко задан по умолчанию.
	 *
	 * @param {string} id css идентификатор связанного dom объекта.
	 *                 Если такого объекта не найдено, он будет создан.
	 * @param {number} x горизонтальная позиция объекта.
	 * @param {number} y вертикальная позиция объекта.
	 * @param {string} value данные, которые будут показаны на объекте
	 */
	constructor(id, x, y, value) {
		this.pos = new Vector(x, y);
		this.size = new Vector(BOX_SIZE, BOX_SIZE);
		this.value = value;
		this.immortal = false;
		this.$parent = document.getElementById(MAIN_FRAME_ID);
		this.$el = document.getElementById(id);
		if (!this.$el) {
			this.$el = this.create(id);
		}
		this.dead = false;
		this.syncDom();
	}

	/**
	 * Синхронизировать состояние объекта с DOM
	 *
	 * Метод обновляет координаты x, у и текст внутри элемента.
	 */
	syncDom() {
		this.$el.style.bottom=`${this.pos.y-this.size.y}px`;
		this.$el.style.left=`${this.pos.x}px`;
		if (this.value && this.$el.innerText !== this.value) {
			this.$el.innerText = this.value;
		}
		const elWidth = parseInt(this.$el.style.width) + DEFAULT_BORDER_WIDTH_X;
		const elHeight = parseInt(this.$el.style.height) + DEFAULT_BORDER_WIDTH_Y;
		if (elWidth !== this.size.x) {
			this.$el.style.width = `${this.size.x-DEFAULT_BORDER_WIDTH_X}px`;
		}
		if (elHeight !== this.size.y) {
			this.$el.style.height = `${this.size.y-DEFAULT_BORDER_WIDTH_Y}px`;
		}
	}

	/**
	 * Вычисляет точку в центре блока.
	 *
	 * @return {Vector} позиция в центре блока
	 */
	center() {
		return new Vector(
			this.pos.x + this.size.x / 2,
			this.pos.y - this.size.y / 2,
		);
	}

	/**
	 * Создаёт элемент в DOM
	 *
	 * @param {string} id css идентификатор связанного dom объекта.
	 * @return {HTMLElement} позиция в центре блока
	 */
	create(id) {
		const el = document.createElement('div');
		el.setAttribute('id', id);
		el.setAttribute('class', 'block');
		this.$parent.appendChild(el);
		return el;
	}

	/**
	 * Удаляет блок из DOM и помечает его как "мёртвый".
	 */
	remove() {
		this.$el.remove();
		this.dead = true;
	}

	/**
	 * Обрабатывает удар мяча по блоку
	 *
	 * @param {Ball} villain мяч, который бьёт по блоку
	 */
	hit(villain) {
		const value = parseInt(this.value) - villain.value;
		if (value < 1) {
			this.remove();
		} else {
			this.value = String(value);
		}
		this.syncDom();
	}

	/**
	 * Проверяем столкновение какого-либо движущегося объекта с нашим.
	 *
	 * Фактически, столкнуться движущийся с неподвижным объектом может только
	 * теми стенками, которые расположены по направлению движения, так что мы
	 * берём две точки на середине этих сторон и проверяем, находятся ли они в
	 * пространстве нашего блока и, если да, вызываем обработчик для этого случая.
	 *
	 * @param {Ball} villain мяч, который бьёт по блоку
	 */
	collide(villain) {
		const vertical = new Vector(
			villain.center().x,
			villain.center().y + villain.velocity.y * villain.size.y/2,
		);
		const horizontal = new Vector(
			villain.center().x + villain.velocity.x * villain.size.x/2,
			villain.center().y,
		);
		let hit = false;
		if (Object.checkCollision(vertical, this.pos, this.size)) {
			if (villain.value < parseInt(this.value) + 1 || this.immortal) {
				villain.velocity.y = villain.velocity.y * -1;
			}
			hit = true;
		}
		if (Object.checkCollision(horizontal, this.pos, this.size)) {
			if (villain.value < parseInt(this.value) + 1 || this.immortal) {
				villain.velocity.x = villain.velocity.x * -1;
			}
			hit = true;
		}
		if (hit) {
			this.hit(villain);
			if (this.immortal) {
				hitPlatformSound.play().catch(console.error);
			} else {
				hitBlockSound.play().catch(console.error);
			}
		}
	}

	/**
	 * ОЛогика проверки на столкновение с прямоугольником
	 *
	 * @param {Vector} point точка, которую мы проверяем на попадание в область
	 * @param {Vector} start верхняя левая точка прямоугольника
	 * @param {Vector} size размер прямоугольника
	 * @return {Boolean} попадёт ли point в прямоугольник,
	 *                   образованный векторами start и size
	 */
	static checkCollision(point, start, size) {
		return point.x >= start.x && point.x <= (start.x + size.x) &&
			point.y <= start.y && point.y >= (start.y - size.y);
	}
}
