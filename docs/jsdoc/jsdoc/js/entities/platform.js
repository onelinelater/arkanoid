import {Object} from './object';
import {Vector} from './vector';
import {Bonus} from './bonus';
import {
	BOX_SIZE,
	SCREEN_WIDTH,
	DEFAULT_PLATFORM_VELOCITY,
	DEFAULT_PLATFORM_WIDTH,
	hitPlatformSound,
	boostSound,
} from '../defaults';


/**
 * Класс "платформа".
 *
 * @alias Platform
 * @augments Object
 * @memberof entities
 */
export class Platform extends Object {
	/** @inheritdoc */
	constructor(id, x, y, value) {
		super(id, x, y, value);
		this.size = new Vector(DEFAULT_PLATFORM_WIDTH, BOX_SIZE);
		this.syncDom();
		this.velocity_x = 0;
		this['immortal'] = true;
	}

	/** @inheritdoc */
	create(id) {
		const el = super.create(id);
		el.classList.add('platform');
		return el;
	}

	/**
	 * Обрабатывает удар мяча по блоку
	 *
	 * В случае, если по платформе попадает бонус,
	 * играет приятная музыка и бонус активируется.
	 *
	 * @param {Ball | Bonus} villain мяч, который не нанаосит вреда или бонус,
	 *                       который даёт профит
	 */
	hit(villain) {
		if (villain instanceof Bonus) {
			boostSound.play().catch(console.error);
			villain.detonate();
		}
	}

	/**
	 * Метод, отвечающий за поведение платформы: перемещение и "отбитие" мяча
	 *
	 * Действует "легальный чит": если мяч "отбивается"
	 * платформой, он всегда летит вверх
	 *
	 * @param {Ball} ball мяч
	 */
	update(ball) {
		if (this.velocity_x !== 0) {
			const resultPos = this.pos.x + this.velocity_x;
			if (resultPos > 0 && resultPos < SCREEN_WIDTH - this.size.x) {
				this.pos.x +=this.velocity_x;
			} else if (this.velocity_x < 0 && this.pos.x > 0) {
				this.pos.x = 0;
			} else if (
				this.velocity_x > 0 &&
				this.pos.x < SCREEN_WIDTH - this.size.x
			) {
				this.pos.x = SCREEN_WIDTH - this.size.x;
			}

			if (Platform.checkCollision(
				ball.center(),
				new Vector(this.pos.x-BOX_SIZE/2, this.pos.y+BOX_SIZE/2),
				new Vector(this.size.x+BOX_SIZE, this.size.y+BOX_SIZE),
			)) {
				hitPlatformSound.play().catch(console.error);
				ball.velocity.y = ball.velocity.y > 0 ?
					ball.velocity.y :
					-ball.velocity.y;
				if (
					ball.velocity.x > 0 && this.velocity_x < 0 ||
					ball.velocity.x < 0 && this.velocity_x > 0
				) {
					ball.velocity.x *= -1;
				}
				this.pos.x -= this.velocity_x;
			}

			this.syncDom();
		}
	}
}


/**
 * Класс "платформа с авто-управлением".
 *
 * @alias AutoPlatform
 * @augments Platform
 * @memberof entities
 */
export class AutoPlatform extends Platform {
	/**
	 * Метод, отвечающий за поведение автоплатформы.
	 *
	 * Платформы вычисляет собстивенные центр и
	 * центр мяча и пытается их совместить в рамках собственной скорости
	 *
	 * @param {Ball} ball мяч
	 */
	update(ball) {
		const center = this.center();
		const ballCenter = ball.center();
		if (Math.abs(ballCenter.x - center.x) > DEFAULT_PLATFORM_VELOCITY) {
			if (center.x < ballCenter.x && center.x < SCREEN_WIDTH-this.size.x/2) {
				this.pos.x += DEFAULT_PLATFORM_VELOCITY;
			} else if (center.x > ballCenter.x && center.x > this.size.x/2) {
				this.pos.x -= DEFAULT_PLATFORM_VELOCITY;
			}
		}
		this.syncDom();
	}
}
