import {Stage} from './abstract';
import {changeStage} from '../index.js';
import {
	Platform,
	Ball,
	Block,
	HealPlatformBonus,
	LongPlatformBonus,
	BallDamageBonus,
} from '../entities/index';
import maps from '../maps/index';
import {
	GAME_STAGE,
	WIN_STAGE,
	LOSE_STAGE,
	OVER_STAGE,
	BOX_SIZE,
	DEFAULT_PLATFORM_HP,
	DEFAULT_PLATFORM_ID,
	DEFAULT_PLATFORM_X_POS,
	DEFAULT_PLATFORM_Y_POS,
	DEFAULT_PLATFORM_VELOCITY,
	DEFAULT_BALL_ID,
	DEFAULT_BALL_DAMAGE,
	DEFAULT_BALL_X_POS,
	DEFAULT_BALL_Y_POS,
	BONUS_SPAWN_PROBABILITY,
	UPDATE_TIMEOUT,
	getRandomInt,
	winSound,
	gameOverSound,
	loseSound,
	SCREEN_COL,
	SCREEN_ROW,
} from '../defaults';


/**
 * Класс "Этап "Игра"".
 *
 * @alias GameStage
 * @augments Stage
 * @memberof stages
 */
export class GameStage extends Stage {
	static stage = GAME_STAGE;

	static transitions = [
		WIN_STAGE,
		LOSE_STAGE,
		OVER_STAGE,
	]

	/** @inheritdoc */
	constructor() {
		super();
		this.objects = [];
		this.bonus_list = [];
		this.loop = null;
	}

	/** @inheritdoc */
	start(config) {
		const self = this;
		this.level = config.level;

		this.available_bonus_list = [
			HealPlatformBonus,
			LongPlatformBonus,
			BallDamageBonus,
		];

		this.platform = new Platform(
			DEFAULT_PLATFORM_ID,
			DEFAULT_PLATFORM_X_POS,
			DEFAULT_PLATFORM_Y_POS,
			DEFAULT_PLATFORM_HP,
		);
		this.objects.push(this.platform);
		this.ball = new Ball(
			DEFAULT_BALL_ID,
			DEFAULT_BALL_X_POS,
			DEFAULT_BALL_Y_POS,
			DEFAULT_BALL_DAMAGE,
		);
		this.ball.bottomHandler = () => {
			let hp = parseInt(self.platform.value);
			hp--;
			self.platform.value = String(hp);
			self.platform.syncDom();
		};
		this.loadMap(maps[this.level]);
		this.initKeyControl();
		this.switch();
	}

	/** @inheritdoc */
	stop() {
		clearInterval(this.loop);
		this.loop = null;
		this.objects = [];
		this.platform = null;
		this.ball = null;
		this.$parent.innerHTML = '';
		this.available_bonus_list = [];
		this.bonus_list = [];
		window.removeEventListener('keydown', this.keydown_handler);
		window.removeEventListener('keyup', this.keyup_handler);
	}

	/** @inheritdoc */
	initKeyControl() {
		const self = this;
		this.keydown_handler = function(event) {
			if (event.code === 'Space' && !event.repeat) {
				self.switch();
			}
			if (self.loop) {
				if (event.code === 'KeyA') {
					self.platform.velocity_x = -DEFAULT_PLATFORM_VELOCITY;
				}
				if (event.code === 'KeyD') {
					self.platform.velocity_x = DEFAULT_PLATFORM_VELOCITY;
				}
			}
		};
		this.keyup_handler = function(event) {
			if (!self.loop) {
				return;
			}
			if (event.code === 'KeyA' && self.platform.velocity_x < 0) {
				self.platform.velocity_x = 0;
			}
			if (event.code === 'KeyD' && self.platform.velocity_x > 0) {
				self.platform.velocity_x = 0;
			}
		};
		window.addEventListener('keydown', this.keydown_handler);
		window.addEventListener('keyup', this.keyup_handler);
	}

	/**
	 * Останавливает игровой процесс
	 */
	switch() {
		if (this.loop) {
			clearInterval(this.loop);
			this.loop = null;
		} else {
			const self = this;
			this.loop = setInterval(function() {
				self.mainLoop();
			}, UPDATE_TIMEOUT);
		}
	}

	/**
	 * Загружает в память карту уровня и создаёт блоки
	 *
	 * @param {string} map карта уровня.
	 */
	loadMap(map) {
		const arr = map.split('\n').map((item)=>item.split(' '));
		const obj = [];
		for (let i=0; i<SCREEN_ROW; i++) {
			for (let j=0; j<SCREEN_COL; j++) {
				if (arr[i][j] > 0) {
					obj.push(
						new Block(
							`block_${obj.length+1}`,
							j*BOX_SIZE, i*BOX_SIZE, arr[i][j],
						),
					);
				}
			}
		}
		this.objects = this.objects.concat(obj);
	}

	/**
	 * Основной игровой цикл.
	 *
	 * 1) Все блоки и платформа проверяются на сталкновение с мячом.
	 * 2) Запускается логика бонусов и проверка их на столкновение с платформой.
	 * 3) Запускается логика мяча
	 * 4) Запускается логика платформы
	 * 5) Проверка на спавн бонусов на месте уничтоженных блоков
	 * 6) Очищаются уничтодженные блоки и бонусы
	 * 7) Проверка проигрыша/выигрыша
	 */
	mainLoop() {
		const self = this;
		this.objects.forEach((item)=>{
			item.collide(this.ball);
		});
		this.bonus_list.forEach((item)=>{
			item.update(self);
		});
		this.bonus_list.forEach((item)=>{
			this.platform.collide(item);
		});
		this.ball.update();
		this.platform.update(this.ball);

		const dead = this.objects.filter((object)=>object.dead);
		if (dead.length) {
			this.spawnBonus(dead[getRandomInt(dead.length)].pos);
		}

		this.objects = this.objects.filter((object)=>!object.dead);
		this.bonus_list = this.bonus_list.filter((object)=>!object.dead);

		if (this.objects.length < 2) {
			if (this.level+1 < maps.length) {
				winSound.play().catch(console.error);
				changeStage(WIN_STAGE, {level: this.level});
			} else {
				gameOverSound.play().catch(console.error);
				changeStage(OVER_STAGE);
			}
		} else if (parseInt(this.platform.value) <= 0) {
			loseSound.play().catch(console.error);
			changeStage(LOSE_STAGE);
		}
	}

	/**
	 * Загружает в память карту уровня и создаёт блоки
	 *
	 * @param {Vector} pos позиция уничтоженного блока.
	 */
	spawnBonus(pos) {
		if (getRandomInt(100) < BONUS_SPAWN_PROBABILITY) {
			const BonusClass = this.available_bonus_list[
				getRandomInt(this.available_bonus_list.length)
			];
			const today = new Date();
			this.bonus_list.push(
				new BonusClass(
					today.getMilliseconds() + this.bonus_list.length,
					pos.x, pos.y,
				),
			);
		}
	}
}
