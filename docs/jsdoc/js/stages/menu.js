import {Stage} from './abstract';
import {changeStage} from '../index.js';
import {GAME_STAGE, MENU_STAGE} from '../defaults';


/**
 * Класс "Этап "Меню"".
 *
 * @alias MenuStage
 * @augments Stage
 * @memberof stages
 */
export class MenuStage extends Stage {
	static stage = MENU_STAGE;

	static transitions = [
		GAME_STAGE,
	]

	/** @inheritdoc */
	constructor() {
		super();
	}

	/** @inheritdoc */
	start(config) {
		this.$parent.innerHTML = '<h1>ARCANOID</h1>\n' +
			'<button id="start" class="play-button">Start</button>\n' +
			'<div class="hint"><ul style="width: 250px"><li>' +
			'<div class="key-example">A</div>\n' +
			'<label>move platform to left</label></li>' +
			'<li><div class="key-example">D</div>\n' +
			'<label>move platform to right</label></li>' +
			'<li><div class="key-example key-example-long">\n' +
			'Space</div><label>pause game</label>' +
			'</li></ul><ul style="width: 490px"><li>\n' +
			'<div class="block bonus D">D</div>\n' +
			'<label>it is damage bonus. It makes ball more dangerous</label>\n' +
			'</li><li><div class="block bonus H">H</div>\n' +
			'<label>it is heal bonus. It restores platform durability</label>\n' +
			'</li><li><div class="block bonus L">L</div>\n' +
			'<label>it is enlargement bonus. It makes platform larger</label>' +
			'</li></ul></div>';
		this.initKeyControl();
	}

	/** @inheritdoc */
	stop() {
		this.$parent.innerHTML = '';
	}

	/** @inheritdoc */
	initKeyControl() {
		const btn = document.getElementById('start');
		btn.onclick = function() {
			changeStage(GAME_STAGE, {level: 0});
		};
	}
}
