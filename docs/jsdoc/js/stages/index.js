/**
 * Этапы, состояния приложения
 * @namespace stages
 */

export {EditStage} from './edit';
export {MenuStage} from './menu';
export {GameStage} from './game';
export {WinStage} from './win';
export {LoseStage} from './lose';
export {OverStage} from './over';
