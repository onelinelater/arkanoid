import {Stage} from './abstract';
import {changeStage} from '../index.js';
import {LOSE_STAGE, MENU_STAGE} from '../defaults';


/**
 * Класс "Этап "Игра проиграна"".
 *
 * @alias LoseStage
 * @augments Stage
 * @memberof stages
 */
export class LoseStage extends Stage {
	static stage = LOSE_STAGE;

	static transitions = [
		MENU_STAGE,
	]

	/** @inheritdoc */
	constructor() {
		super();
	}

	/** @inheritdoc */
	start(config) {
		this.$parent.innerHTML = '<h1 style="padding-top: 200px;">GAME OVER!</h1>' +
			'<button id="start" class="play-button">Try again</button>\n';
		this.initKeyControl();
	}

	/** @inheritdoc */
	stop() {
		this.$parent.innerHTML = '';
	}

	/** @inheritdoc */
	initKeyControl() {
		const btn = document.getElementById('start');
		btn.onclick = function() {
			changeStage(MENU_STAGE, {level: 0});
		};
	}
}
