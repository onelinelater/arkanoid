import {Object} from './object';
import {Vector} from './vector';
import {SCREEN_WIDTH, SCREEN_HEIGHT, DEFAULT_BULLET_SPEED} from '../defaults';


/**
 * Класс "мяч".
 *
 * @alias Ball
 * @augments Object
 * @memberof entities
 */
export class Ball extends Object {
	/**
	 * Дополнительно определяет скорость и обработчик касания нижней границы.
	 *
	 * @inheritdoc
	 */
	constructor(id, x, y, value) {
		super(id, x, y, value);
		this.velocity = new Vector(-1, 1);
		this.bottomHandler = () => {};
	}

	/**
	 * Добавляет css класс ball.
	 *
	 * @inheritdoc
	 */
	create(id) {
		const el = super.create(id);
		el.classList.add('ball');
		return el;
	}

	/**
	 * Мяч отскакивает от боковых стен и верхней стенки.
	 * При соприкосновении с нижней стенкой вызввает переданный
	 * извне обработчик.
	 *
	 * @inheritdoc
	 */
	update() {
		this.pos.x = this.pos.x + this.velocity.x;
		this.pos.y = this.pos.y + this.velocity.y;
		if (this.pos.x > SCREEN_WIDTH - this.size.x) {
			this.velocity.x = -DEFAULT_BULLET_SPEED;
		}
		if (this.pos.x < 0 ) {
			this.velocity.x = DEFAULT_BULLET_SPEED;
		}
		if (this.pos.y > SCREEN_HEIGHT) {
			this.velocity.y = -DEFAULT_BULLET_SPEED;
		}
		if (this.pos.y-this.size.y < 0 ) {
			this.velocity.y = DEFAULT_BULLET_SPEED;
			this.bottomHandler();
		}
		this.syncDom();
	}
}
