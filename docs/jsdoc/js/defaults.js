/** идентификатор тега, где будет встроена игровая логика
 * @default main_frame
 * */
export const MAIN_FRAME_ID='main_frame';
/**
 * пауза между обновлениями логики
 * @default 5
 * */
export const UPDATE_TIMEOUT = 5;

/**
 * Размер блока в пикселах
 * @default 20
 * */
export const BOX_SIZE = 20;
/**
 * Ширина игрового окна в пикселах
 * @default 800
 * */
export const SCREEN_WIDTH = 800;
/**
 * SCREEN_HEIGHT Высота игрового окна в пикселах
 * @default 600
 * */
export const SCREEN_HEIGHT = 600;

/** Количество блоков, которое влезает в строку */
export const SCREEN_COL = Math.floor(SCREEN_WIDTH/BOX_SIZE);
/** Количество блоков, которое влезает в столбец */
export const SCREEN_ROW = Math.floor(SCREEN_HEIGHT/BOX_SIZE);

/** Константы для генератора уровней */

/**
 * Количество блоков, которые отступит генератор от края поля
 * @default 1
 * */
export const GEN_OFFSET = 1;
/**
 * Количество блоков снизу, которые не будут заполнены
 * @default 20
 * */
export const GEN_FREE_SPACE = 20;

/** Ключи для реализованных этапов игры */

/**
 * Этап "игра"
 * @default play
 * */
export const GAME_STAGE = 'play';
/**
 * Этап "меню"
 * @default menu
 * */
export const MENU_STAGE = 'menu';
/**
 * Этап "окно поражения"
 * @default lose
 * */
export const LOSE_STAGE = 'lose';
/**
 *  Этап "уровень закончен успешно"
 *  @default win
 * */
export const WIN_STAGE = 'win';
/**
 * Этап "игра выиграна"
 * @default over
 * */
export const OVER_STAGE = 'over';
/**
 *  Этап по умолчанию, с которого начнётся игра
 *  @default menu
 * */
export const DEFAULT_STAGE = MENU_STAGE;

/**
 * Максимальное здоровье блока
 * @default 3
 * */
export const MAX_BLOCK_HP = 3;
/**
 * Толщина обводки блока справа
 * @default 4
 * */
export const DEFAULT_BORDER_WIDTH_X = 4;
/**
 * Толщина обводки блока снизу
 * @default 6
 * */
export const DEFAULT_BORDER_WIDTH_Y = 6;

/**
 * Идентификатор DOM элемента для платформы
 * @default platform
 * */
export const DEFAULT_PLATFORM_ID = 'platform';
/**
 * Прочность платфоры. Уменьшается, если шарик попадает не на платформу
 * @default 2
 * */
export const DEFAULT_PLATFORM_HP = 2;
/**
 * Максимальная прочность платфоры.
 * @default 20
 * */
export const MAX_PLATFORM_HP = 20;
/**
 * Ширина в пикселах платформы по-умолчанию
 * @default 100
 * */
export const DEFAULT_PLATFORM_WIDTH = 100;
/** Координата X, на которой появится платформа */
export const DEFAULT_PLATFORM_X_POS = SCREEN_WIDTH/2-DEFAULT_PLATFORM_WIDTH/2;
/** Координата Y, на которой появится платформа */
export const DEFAULT_PLATFORM_Y_POS = BOX_SIZE * 1.5;
/**
 * Скорость перемещения платформы
 * @default 5
 * */
export const DEFAULT_PLATFORM_VELOCITY = 5;
/**
 * Длительность действия бонуса на "удлиннение"
 * @default 10000
 * */
export const LONG_PLATFORM_BONUS_TIME = 10 * 1000;
/**
 * Длинна платформы под действеием
 * бонуса на "удлиннение" в пикселах
 * @default 200
 * */
export const LONG_PLATFORM_WIDTH = 200;

/**
 * Идентификатор DOM элемента для мяча
 * @default ball
 * */
export const DEFAULT_BALL_ID = 'ball';
/**
 * Начальный урон мяча
 * @default 1
 * */
export const DEFAULT_BALL_DAMAGE = 1;
/**
 * Максимальный урон мяча
 * @default 3
 * */
export const MAX_BALL_DAMAGE = 3;
/** Координата X, на которой появится мяч */
export const DEFAULT_BALL_X_POS = SCREEN_WIDTH/2-BOX_SIZE/2;
/** Координата Y, на которой появится мяч */
export const DEFAULT_BALL_Y_POS = DEFAULT_PLATFORM_Y_POS + BOX_SIZE;
/**
 * Скорость перемещения мяча
 * @default 1
 * */
export const DEFAULT_BULLET_SPEED = 1;

/**
 * Вероятность выпадения бонуса в процентах
 * @default 25
 * */
export const BONUS_SPAWN_PROBABILITY = 25;

/**
 * Константы для хранения звуков
 * @namespace audio
 * */

/**
 * Звук удара об платформу
 * @type {HTMLAudioElement}
 * @memberof audio
 * @alias audio.hitPlatformSound
 * */
export const hitPlatformSound = new Audio('resources/sound/hit_platform.mp3');
/**
 * Звук удара об блок
 * @type {HTMLAudioElement}
 * @memberof audio
 * @alias audio.hitBlockSound
 * */
export const hitBlockSound = new Audio('resources/sound/hit_block.mp3');
/**
 * Звук получения бонуса
 * @type {HTMLAudioElement}
 * @memberof audio
 * @alias audio.boostSound
 * */
export const boostSound = new Audio('resources/sound/boost.mp3');
/**
 * Звук победы на уровне
 * @type {HTMLAudioElement}
 * @memberof audio
 * @alias audio.winSound
 * */
export const winSound = new Audio('resources/sound/win.mp3');
/**
 * Звук поражения
 * @type {HTMLAudioElement}
 * @memberof audio
 * @alias audio.loseSound
 * */
export const loseSound = new Audio('resources/sound/lose.mp3');
/**
 * Звук победы в игре
 * @type {HTMLAudioElement}
 * @memberof audio
 * @alias audio.gameOverSound
 * */
export const gameOverSound = new Audio('resources/sound/over.mp3');

/**
 * Функция получает случайное целое число от 0 до заданного максимума
 *
 * Не включает максимальное число
 *
 * @ignore
 * @param {number} max максимальная граница
 * @return {number} целое число в диапазоне от 0 до max */
export function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}
