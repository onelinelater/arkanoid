import {
	BOX_SIZE,
	SCREEN_COL,
	SCREEN_ROW,
	GEN_OFFSET,
	GEN_FREE_SPACE,
	MAX_BLOCK_HP,
	SCREEN_WIDTH,
	SCREEN_HEIGHT,
	getRandomInt,
} from '../defaults';

/**
 * Генератор уровней
 * @namespace gen
 */

/**
 * Генерирует случайный уровень.
 *
 * @memberof gen
 * @return {string} Карта уровня в виде текста.
 *                  Строки разделяются переносами, колнки пробелами
 */
function genLevel() {
	let arr = Array.from(Array(SCREEN_WIDTH/BOX_SIZE).keys())
		.map(()=>Array.from(Array(SCREEN_HEIGHT/BOX_SIZE).keys()));
	arr = arr.map((item)=>item.map(()=>0));


	for (let i=GEN_FREE_SPACE; i<SCREEN_ROW-GEN_OFFSET; i++) {
		for (let j=GEN_OFFSET; j<SCREEN_COL-GEN_OFFSET; j++) {
			arr[i][j] = getRandomInt(MAX_BLOCK_HP + 1);
		}
	}
	return arr.map((item)=>item.join(' ')).join('\n');
}

export default (() => {
	return Array.from(Array(10).keys()).map(()=>genLevel());
})();
