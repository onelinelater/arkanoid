module.exports = {
	'env': {
		'browser': true,
		'es2021': true,
	},
	'extends': [
		'google',
	],
	'parser': 'babel-eslint',
	'parserOptions': {
		'ecmaVersion': 12,
		'sourceType': 'module',
	},
	'rules': {
		'indent': ['error', 'tab'],
		'no-tabs': ['error', {allowIndentationTabs: true}],
	},
};
