/**
 * Класс "вектор".
 * @alias Vector
 * @memberof entities
 */
export class Vector {
	/**
	 * Конструктор. Создаёт двумерный вектор.
	 *
	 * @param {number} x координата.
	 * @param {number} y координата.
	 */
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
}
