/**
 * Объекты, которые участвуют в процессе игры
 * @namespace entities
 */

export {Ball} from './ball';
export {AutoPlatform, Platform} from './platform';
export {Block} from './block';
export {
	HealPlatformBonus,
	LongPlatformBonus,
	BallDamageBonus,
} from './bonus';
