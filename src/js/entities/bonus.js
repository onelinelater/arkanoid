import {Object} from './object';
import {Vector} from './vector';
import {
	SCREEN_WIDTH,
	DEFAULT_PLATFORM_WIDTH,
	MAX_BALL_DAMAGE,
	MAX_PLATFORM_HP,
	LONG_PLATFORM_BONUS_TIME,
	LONG_PLATFORM_WIDTH,
} from '../defaults';


/**
 * Класс "Бонус".
 *
 * @alias Bonus
 * @augments Object
 * @abstract
 * @memberof entities
 */
export class Bonus extends Object {
	static bonusType = ''

	/**
	 * Абстрактный бонус.
	 * Дополнительно определяет вертикальную скорость, признак
	 * активности эффекта и проставляет значение в зависимости от
	 * класса бонуса.
	 *
	 * @inheritdoc
	 */
	constructor(id, x, y) {
		super(id, x, y, '');
		this.velocity = new Vector(0, -1);
		this.value = this.constructor.bonusType;
		this.$el.classList.add(this.value);
		this.active = false;
		this.syncDom();
	}

	/**
	 * Добавляет общий css класс bonus.
	 *
	 * @inheritdoc
	 */
	create(id) {
		const el = super.create(id);
		el.classList.add('bonus');
		return el;
	}

	/**
	 * Метод, отвечающий за поведение бонуса.
	 *
	 * Бонус движется вниз, пока не будет активирован платформой или
	 * не достигнет нижней стенки.
	 * Если бонус был активирован, сработает его эффект,
	 * бонус деактивируется и самоуничтожится.
	 * Так же бонус самоуничтожится по достидении нижней стенки.
	 *
	 * @param {GameStage} stage уровень, объекты которого будут
	 *                    преобразованы бонусом
	 */
	update(stage) {
		if (this.active) {
			this.affect(stage);
			this.active = false;
			this.remove();
		} else if (this.pos.y < 0 ) {
			this.remove();
		} else {
			this.pos.y = this.pos.y + this.velocity.y;
			this.syncDom();
		}
	}
	/**
	 * Метод-активатор бонуса
	 */
	detonate() {
		this.active = true;
	}
	/**
	 * Метод-эффект бонуса. Должен быть переопределён в наследниках.
	 *
	 * @param {GameStage} stage уровень, объекты которого будут
	 *                    преобразованы бонусом
	 */
	affect(stage) {
		throw new Error('Не определён метод взаимодействия с миром');
	}
}


/**
 * Класс "Бонус длинная платформа".
 *
 * @alias LongPlatformBonus
 * @augments Bonus
 * @memberof entities
 */
export class LongPlatformBonus extends Bonus {
	static bonusType = 'L'

	/**
	 * Эффект увеличивает длину платформы каждые 5 миллисекунд,
	 * пока он не достигнет установленной константы LONG_PLATFORM_WIDTH.
	 * Эффект продлится LONG_PLATFORM_BONUS_TIME миллисекунд, затем каждые
	 * 5 миллисекунд платформа начнёт уменьшаться, пока не достигнет
	 * стандартной ширины.
	 *
	 * @inheritdoc
	 */
	affect(stage) {
		if (stage.platform.size.x < LONG_PLATFORM_WIDTH) {
			let loop = null;

			/** Увеличивает платформу */
			function increasePlatform() {
				if (stage.platform.size.x < LONG_PLATFORM_WIDTH) {
					stage.platform.size.x += 1;
					if (stage.platform.pos.x > 1) {
						stage.platform.pos.x -= 1;
					}
					stage.platform.syncDom();
				} else {
					clearInterval(loop);
					loop = null;
					setTimeout(()=>{
						loop = setInterval(decreasePlatform, 5);
					}, LONG_PLATFORM_BONUS_TIME);
				}
			}

			/** Уменьшает платформу */
			function decreasePlatform() {
				if (stage.platform.size.x > DEFAULT_PLATFORM_WIDTH) {
					stage.platform.size.x -= 1;
					if (stage.platform.pos.x < SCREEN_WIDTH-stage.platform.size.x) {
						stage.platform.pos.x += 1;
					}
					stage.platform.syncDom();
				} else {
					clearInterval(loop);
					clearInterval(loop);
				}
			}
			loop = setInterval(increasePlatform, 5);
		}
	}
}

/**
 * Класс "Бонус урона мяча".
 *
 * @alias BallDamageBonus
 * @augments Bonus
 * @memberof entities
 */
export class BallDamageBonus extends Bonus {
	static bonusType = 'D'

	/**
	 * Эффект увеличивает урон мяча на еденицу. Максимальный урон ограничен
	 * и записан в константе MAX_BALL_DAMAGE
	 *
	 * @inheritdoc
	 */
	affect(stage) {
		if (parseInt(stage.ball.value) < MAX_BALL_DAMAGE) {
			stage.ball.value += 1;
			stage.ball.syncDom();
		}
	}
}

/**
 * Класс "Бонус прочности платформы".
 *
 * @alias HealPlatformBonus
 * @augments Bonus
 * @memberof entities
 */
export class HealPlatformBonus extends Bonus {
	static bonusType = 'H'

	/**
	 * Эффект увеличивает прочность платформы на еденицу.
	 * Максимальная прочность ограничена и записана в константе MAX_PLATFORM_HP.
	 *
	 * @inheritdoc
	 */
	affect(stage) {
		let val = parseInt(stage.platform.value);
		if (val < MAX_PLATFORM_HP) {
			val += 1;
			stage.platform.value = String(val);
			stage.platform.syncDom();
		}
	}
}
