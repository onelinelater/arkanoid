import {Stage} from './abstract';
import {Block} from '../entities/index';
import {BOX_SIZE, SCREEN_WIDTH, SCREEN_HEIGHT} from '../defaults';


/**
 * Класс "Этап "Редактор уровня"".
 *
 * @alias EditStage
 * @augments Stage
 * @memberof stages
 */
export class EditStage extends Stage {
	/** @inheritdoc */
	constructor() {
		super();
		this.objects = [];
		this.type = 1;
		this.shadow = null;
	}

	/** @inheritdoc */
	start(config) {
		this.shadow = new Block(`shadow`, 0, 0, this.type);
		this.initKeyControl();
	}

	/**
	 * Сохранить текущее положение блоков. Выводит его в консоль.
	 */
	saveMap() {
		let arr = Array.from(Array(SCREEN_HEIGHT/BOX_SIZE).keys())
			.map(()=>Array.from(Array(SCREEN_WIDTH/BOX_SIZE).keys()));
		arr = arr.map((item)=>item.map(()=>0));
		this.objects.forEach((item)=>{
			const j = item.pos.x / BOX_SIZE;
			const i = item.pos.y / BOX_SIZE;
			arr[i][j] = item.value;
		});
		console.log(arr.map((item)=>item.join(' ')).join('\n'));
	}

	/** @inheritdoc */
	initKeyControl() {
		const self = this;
		window.addEventListener('keydown', function(event) {
			if (event.code === 'KeyS') {
				self.saveMap();
			}
			if (event.code === 'KeyD') {
				self.shadow.$el.classList.remove(Block.options[self.type]);
				self.type += 1;
				self.shadow.value = self.type;
				const cssClasses = self.shadow.$el.getAttribute('class');
				self.shadow.$el.setAttribute(
					'class',
					`${cssClasses} ${Block.options[self.shadow.value]}`,
				);
				self.shadow.syncDom();
			}
			if (event.code === 'KeyA') {
				self.shadow.$el.classList.remove(Block.options[self.type]);
				self.type -= 1;
				self.shadow.value = self.type;
				const cssClasses = self.shadow.$el.getAttribute('class');
				self.shadow.$el.setAttribute(
					'class',
					`${cssClasses} ${Block.options[self.shadow.value]}`,
				);
				self.shadow.syncDom();
			}
		});

		self.$parent.addEventListener('mousemove', (e) => {
			const rect = self.$parent.getBoundingClientRect();
			let x = Math.floor((e.clientX - rect.left)/BOX_SIZE) * BOX_SIZE;
			let y = (
				Math.floor(
					(SCREEN_HEIGHT-e.clientY - rect.top)/BOX_SIZE,
				) * BOX_SIZE + BOX_SIZE*2
			);
			x = x >= 0 ? x : self.shadow.pos.x;
			y = y >= 0 ? y : self.shadow.pos.y;
			if (self.shadow.pos.x !== x || self.shadow.pos.y !== y) {
				self.shadow.pos.x = x;
				self.shadow.pos.y = y;
				self.shadow.syncDom();
			}
		});
		this.$parent.addEventListener('click', () => {
			this.shadow.$el.id = `block_${this.objects.length+1}`;
			this.objects.push(this.shadow);
			this.shadow = new Block(`shadow`, 0, 0, this.type);
		});
	}
}
