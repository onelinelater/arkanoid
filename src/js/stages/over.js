import {Stage} from './abstract';
import {changeStage} from '../index.js';
import {OVER_STAGE, MENU_STAGE} from '../defaults';


/**
 * Класс "Этап "Игра пройдена"".
 *
 * @alias OverStage
 * @augments Stage
 * @memberof stages
 */
export class OverStage extends Stage {
	static stage = OVER_STAGE;

	static transitions = [
		MENU_STAGE,
	]

	/** @inheritdoc */
	constructor() {
		super();
	}

	/** @inheritdoc */
	start(config) {
		this.$parent.innerHTML = '<h1 style="padding-top: 200px;">YOU WIN!</h1>' +
			'<button id="start" class="play-button">Return to menu</button>\n';
		this.initKeyControl();
	}

	/** @inheritdoc */
	stop() {
		this.$parent.innerHTML = '';
	}

	/** @inheritdoc */
	initKeyControl() {
		const btn = document.getElementById('start');
		btn.onclick = function() {
			changeStage(MENU_STAGE, {level: 0});
		};
	}
}
